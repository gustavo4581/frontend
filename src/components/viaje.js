import React, { Component } from "react";
import ViajeDataService from "../services/viaje.service";

export default class Viaje extends Component {
    constructor(props) {
        super(props);
        this.onChangeCodigoViaje = this.onChangeCodigoViaje.bind(this);
        this.onChangeNumeroPlazas = this.onChangeNumeroPlazas.bind(this);
        this.onChangeLugarOrigen = this.onChangeLugarOrigen.bind(this);
        this.onChangeDestino = this.onChangeDestino.bind(this);
        this.onChangePrecio = this.onChangePrecio.bind(this);
        this.getViaje = this.getViaje.bind(this);
        this.updateViaje = this.updateViaje.bind(this);
        this.deleteViaje = this.deleteViaje.bind(this);

        this.state = {
            currentViaje: {
                id: null,
                coidgo_viaje: "",
                numero_plazas: 0,
                lugat_origen: "",
                destino: "",
                precio: ""
            },
        };
    }

    componentDidMount() {
        this.getViaje(this.props.match.params.id);
    }

    onChangeCodigoViaje(e) {
        const codigoViaje = e.target.value;

        this.setState(function(prevState) {
            return {
                currentViaje: {
                    ...prevState.currentViaje,
                    codigo_viaje: codigoViaje
                }
            };
        });
    }
    onChangeNumeroPlazas(e) {
        const numeroPlazas = e.target.value;

        this.setState(function(prevState) {
            return {
                currentViaje: {
                    ...prevState.currentViaje,
                    numero_plazas: numeroPlazas
                }
            };
        });
    }
    onChangeLugarOrigen(e) {
        const lugarOrigen = e.target.value;

        this.setState(function(prevState) {
            return {
                currentViaje: {
                    ...prevState.currentViaje,
                    lugar_origen: lugarOrigen
                }
            };
        });
    }
    onChangeDestino(e) {
        const destino = e.target.value;

        this.setState(function(prevState) {
            return {
                currentViaje: {
                    ...prevState.currentViaje,
                    destino: destino
                }
            };
        });
    }
    onChangePrecio(e) {
        const precio = e.target.value;

        this.setState(function(prevState) {
            return {
                currentViaje: {
                    ...prevState.currentViaje,
                    precio: precio
                }
            };
        });
    }

    getViaje(id) {
        ViajeDataService.get(id)
            .then(response => {
                this.setState({
                    currentViaje: response.data
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    updateViaje() {
        ViajeDataService.update(
            this.state.currentViaje.id,
            this.state.currentViaje
        )
            .then(response => {
                console.log(response.data);
                this.setState({
                    message: "El Viaje fue actualizado exitosamente!"
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    deleteViaje() {
        ViajeDataService.delete(this.state.currentViaje.id)
            .then(response => {
                console.log(response.data);
                this.props.history.push('/viajes')
            })
            .catch(e => {
                console.log(e);
            });
    }

    render() {
        const { currentViaje } = this.state;

        return (
            <div>
                {currentViaje ? (
                    <div className="edit-form">
                        <h4>Viaje</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="codigo_viaje">Codigo del Viaje</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="codigo_viaje"
                                    value={currentViaje.codigo_viaje}
                                    onChange={this.onChangeCodigoViaje}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="numero_plazas">No de plazas</label>
                                <input
                                    type="number"
                                    className="form-control"
                                    id="numero_plazas"
                                    value={currentViaje.numero_plazas}
                                    onChange={this.onChangeNumeroPlazas}
                                />
                            </div>

                            <div className="form-group">
                                <label htmlFor="luegar_origen">Lugar de Origen</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="lugar_origen"
                                    value={currentViaje.lugar_origen}
                                    onChange={this.onChangeLugarOrigen}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="destino">Destino</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="destino"
                                    value={currentViaje.destino}
                                    onChange={this.onChangeDestino}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="precio">Precio</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="preio"
                                    value={currentViaje.precio}
                                    onChange={this.onChangePrecio}
                                />
                            </div>
                        </form>
                        <button
                            className="badge badge-danger mr-2"
                            onClick={this.deleteViaje}
                        >
                            Delete
                        </button>

                        <button
                            type="submit"
                            className="badge badge-success"
                            onClick={this.updateViaje}
                        >
                            Update
                        </button>
                    </div>
                ) : (
                    <div>
                        <br />
                        <p>Porfavor haga click en un viaje para mas opciones..</p>
                    </div>
                )}
            </div>
        );
    }
}
