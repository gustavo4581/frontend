import React, { Component } from "react";
import ViajeroDataService from "../services/viajero.service";

export default class AddViajero extends Component {
  constructor(props) {
    super(props);
    this.onChangeNombre = this.onChangeNombre.bind(this);
    this.onChangeCedula = this.onChangeCedula.bind(this);
    this.onChangeFechaNacimiento = this.onChangeFechaNacimiento.bind(this);
    this.onChangeTelefono = this.onChangeTelefono.bind(this);
    this.saveViajero = this.saveViajero.bind(this);
    this.newViajero = this.newViajero.bind(this);

    this.state = {
      currentViajero: {
        id: null,
        nombre: "",
        cedula: "",
        fecha_nacimiento: "",
        telefono: "",
      },
    };
  }

  onChangeNombre(e) {
    this.setState({
      nombre: e.target.value
    });
  }

  onChangeCedula(e) {
    this.setState({
      cedula: e.target.value
    });
  }

  onChangeFechaNacimiento(e) {
    this.setState({
      fecha_nacimiento: e.target.value
    });
  }

  onChangeTelefono(e) {
    this.setState({
      telefono: e.target.value
    });
  }

  saveViajero() {

    var data = {
      nombre: this.state.nombre,
      cedula: this.state.cedula,
      fecha_nacimiento: this.state.fecha_nacimiento,
      telefono: this.state.telefono
    };


    ViajeroDataService.create(data)
      .then(response => {
        this.setState({
          id: response.data.id,
          nombre: response.data.nombre,
          cedula: response.data.cedula,
          fecha_nacimiento: response.data.fecha_nacimiento,
          telefono: response.data.telefono,
          submitted: true
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }
  goToViajeros() {
    this.props.history.push('/viajeros')
  }
  newViajero() {
    this.setState({
      id: null,
      nombre: "",
      cedula: '',
      fecha_nacimiento: "",
      telefono: "",

      submitted: false
    });
  }

  render() {
    return (
      <div className="submit-form">
        {this.state.submitted ? (
          <div>
            <h4>Viajero agregado exitosamente!</h4>
            <button className="btn btn-success" onClick={this.newViajero}>
              Agregar nuevo
            </button>
          </div>
        ) : (
          <div>
            <div className="form-group">
              <label htmlFor="nombre">Nombre</label>
              <input
                type="text"
                className="form-control"
                id="nombre"
                required
                value={this.state.nombre}
                onChange={this.onChangeNombre}
                name="nombre"
              />
            </div>

            <div className="form-group">
              <label htmlFor="cedula">Cedula</label>
              <input
                type="text"
                className="form-control"
                id="cedula"
                required
                value={this.state.cedula}
                onChange={this.onChangeCedula}
                name="cedula"
              />
            </div>
            <div className="form-group">
              <label htmlFor="fecha_nacimiento">Fecha de Nacimiento</label>
              <input
                  type="text"
                  className="form-control"
                  id="fecha_nacimiento"
                  required
                  value={this.state.fecha_nacimiento}
                  onChange={this.onChangeFechaNacimiento}
                  name="fecha_nacimiento"
              />
            </div>
            <div className="form-group">
              <label htmlFor="telefono">Telefono</label>
              <input
                  type="text"
                  className="form-control"
                  id="telefono"
                  required
                  value={this.state.telefono}
                  onChange={this.onChangeTelefono}
                  name="description"
              />
            </div>

            <button onClick={this.saveViajero} className="btn btn-success">
              Guardar
            </button>
          </div>
        )}
      </div>
    );
  }
}
