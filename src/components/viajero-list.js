import React, { Component } from "react";
import ViajeroDataService from "../services/viajero.service";
import { Link } from "react-router-dom";

export default class ViajeroList extends Component {
  constructor(props) {
    super(props);
    this.retrieveViajeros = this.retrieveViajeros.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveViajero = this.setActiveViajero.bind(this);

    this.state = {
      viajeros: [],
      currentViajero: null,
      currentIndex: -1,
    };
  }

  componentDidMount() {
    this.retrieveViajeros();
  }

  retrieveViajeros() {
    ViajeroDataService.getAll()
      .then(response => {
        this.setState({
          viajeros: response.data
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  refreshList() {
    this.retrieveViajeros();
    this.setState({
      currentViajero: null,
      currentIndex: -1
    });
  }

  setActiveViajero(viajero, index) {
    this.setState({
      currentViajero: viajero,
      currentIndex: index
    });
  }
  render() {
    const { viajeros, currentViajero, currentIndex } = this.state;

    return (
      <div className="list row">
        <div className="col-md-6">
          <h4>Lista de Viajeros</h4>

          <ul className="list-group">
            {viajeros &&
            viajeros.map((viajero, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveViajero(viajero, index)}
                  key={index}
                >
                  {viajero.nombre}
                </li>
              ))}
          </ul>
        </div>
        <div className="col-md-6">
          {currentViajero ? (
            <div>
              <h4>Viajero</h4>
              <div>
                <label>
                  <strong>Nombre:</strong>
                </label>{" "}
                {currentViajero.nombre}
              </div>
              <div>
                <label>
                  <strong>Cedula:</strong>
                </label>{" "}
                {currentViajero.cedula}
              </div>
              <div>
                <label>
                  <strong>Fecha de Nacimiento:</strong>
                </label>{" "}
                {currentViajero.fecha_nacimiento }
              </div>
              <div>
                <label>
                  <strong>Telefono:</strong>
                </label>{" "}
                {currentViajero.telefono }
              </div>

              <Link
                to={"/viajero/" + currentViajero.id}
                className="badge badge-warning"
              >
                Edit
              </Link>
            </div>
          ) : (
            <div>
              <br />
              <p>Porfavor haga click en un viajero para mas opciones..</p>
            </div>
          )}
        </div>
      </div>
    );
  }
}
