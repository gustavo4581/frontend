import React, { Component } from "react";
import ViajeDataService from "../services/viaje.service";

export default class AddViaje extends Component {
    constructor(props) {
        super(props);
        this.onChangeCodigoViaje = this.onChangeCodigoViaje.bind(this);
        this.onChangeNumeroPlazas = this.onChangeNumeroPlazas.bind(this);
        this.onChangeLugarOrigen = this.onChangeLugarOrigen.bind(this);
        this.onChangeDestino = this.onChangeDestino.bind(this);
        this.onChangePrecio = this.onChangePrecio.bind(this);
        this.saveViaje = this.saveViaje.bind(this);
        this.newViaje = this.newViaje.bind(this);

        this.state = {
            currentViaje: {
                id: null,
                coidgo_viaje: "",
                numero_plazas: 0,
                lugat_origen: "",
                destino: "",
                precio: ""
            },
        };
    }

    onChangeCodigoViaje(e) {
        this.setState({
            codigo_viaje: e.target.value
        });
    }

    onChangeNumeroPlazas(e) {
        this.setState({
            numero_plazas: e.target.value
        });
    }

    onChangeLugarOrigen(e) {
        this.setState({
            lugar_origen: e.target.value
        });
    }

    onChangeDestino(e) {
        this.setState({
            destino: e.target.value
        });
    }

    onChangePrecio(e) {
        this.setState({
            precio: e.target.value
        });
    }

    saveViaje() {

        var data = {
            codigo_viaje: this.state.codigo_viaje,
            numero_plazas: this.state.numero_plazas,
            lugar_origen: this.state.lugar_origen,
            destino: this.state.destino,
            precio: this.state.precio
        };


        ViajeDataService.create(data)
            .then(response => {
                this.setState({
                    id: response.data.id,
                    coidgo_viaje: response.data.codigo_viaje,
                    numero_plazas: response.data.numero_plazas,
                    lugar_origen: response.data.lugar_origen,
                    destino: response.data.destino,
                    precio: response.data.precio,
                    submitted: true
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }
    goToViajes() {
        this.props.history.push('/viajes')
    }
    newViaje() {
        this.setState({
            id: null,
            coidgo_viaje: "",
            numero_plazas: 0,
            lugat_origen: "",
            destino: "",
            precio: 0,

            submitted: false
        });
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>Viaje agregado exitosamente!</h4>
                        <button className="btn btn-success" onClick={this.newViaje}>
                            Agregar nuevo
                        </button>
                    </div>
                ) : (
                    <div>
                        <div className="form-group">
                            <label htmlFor="codigo_viaje">Codigo de Viaje</label>
                            <input
                                type="text"
                                className="form-control"
                                id="codigo_viaje"
                                required
                                value={this.state.codigo_viaje}
                                onChange={this.onChangeCodigoViaje}
                                name="codigo_viaje"
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="numero_plazas">No de plazas</label>
                            <input
                                type="number"
                                className="form-control"
                                id="numero_plazas"
                                required
                                value={this.state.numero_plazas}
                                onChange={this.onChangeNumeroPlazas}
                                name="numero_plazas"
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="lugar_origen">Lugar de origen</label>
                            <input
                                type="text"
                                className="form-control"
                                id="lugar_origen"
                                required
                                value={this.state.lugar_origen}
                                onChange={this.onChangeLugarOrigen}
                                name="lugar_origen"
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="destino">Destino</label>
                            <input
                                type="text"
                                className="form-control"
                                id="destino"
                                required
                                value={this.state.destino}
                                onChange={this.onChangeDestino}
                                name="destino"
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="precio">Precio</label>
                            <input
                                type="number"
                                className="form-control"
                                id="precio"
                                required
                                value={this.state.precio}
                                onChange={this.onChangePrecio}
                                name="precio"
                            />
                        </div>

                        <button onClick={this.saveViaje} className="btn btn-success">
                            Guardar
                        </button>
                    </div>
                )}
            </div>
        );
    }
}
