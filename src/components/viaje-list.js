import React, { Component } from "react";
import ViajeDataService from "../services/viaje.service";
import { Link } from "react-router-dom";

export default class ViajeList extends Component {
    constructor(props) {
        super(props);
        this.retrieveViajes = this.retrieveViajes.bind(this);
        this.refreshList = this.refreshList.bind(this);
        this.setActiveViaje = this.setActiveViaje.bind(this);

        this.state = {
            viajes: [],
            currentViaje: null,
            currentIndex: -1,
        };
    }

    componentDidMount() {
        this.retrieveViajes();
    }

    retrieveViajes() {
        ViajeDataService.getAll()
            .then(response => {
                this.setState({
                    viajes: response.data
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    refreshList() {
        this.retrieveViajes();
        this.setState({
            currentViaje: null,
            currentIndex: -1
        });
    }

    setActiveViaje(viaje, index) {
        this.setState({
            currentViaje: viaje,
            currentIndex: index
        });
    }
    render() {
        const { viajes, currentViaje, currentIndex } = this.state;

        return (
            <div className="list row">
                <div className="col-md-6">
                    <h4>Lista de Viajes</h4>

                    <ul className="list-group">
                        {viajes &&
                        viajes.map((viaje, index) => (
                            <li
                                className={
                                    "list-group-item " +
                                    (index === currentIndex ? "active" : "")
                                }
                                onClick={() => this.setActiveViaje(viaje, index)}
                                key={index}
                            >
                                {viaje.codigo_viaje}&nbsp;&nbsp;&nbsp;
                                {viaje.lugar_origen}  -  {viaje.destino}
                            </li>
                        ))}
                    </ul>
                </div>
                <div className="col-md-6">
                    {currentViaje ? (
                        <div>
                            <h4>Viaje</h4>
                            <div>
                                <label>
                                    <strong>Codigo de Viaje:</strong>
                                </label>{" "}
                                {currentViaje.codigo_viaje}
                            </div>
                            <div>
                                <label>
                                    <strong>No Plazas:</strong>
                                </label>{" "}
                                {currentViaje.numero_plazas}
                            </div>
                            <div>
                                <label>
                                    <strong>Lugar de origen:</strong>
                                </label>{" "}
                                {currentViaje.lugar_origen }
                            </div>
                            <div>
                                <label>
                                    <strong>Destino:</strong>
                                </label>{" "}
                                {currentViaje.destino }
                            </div>
                            <div>
                                <label>
                                    <strong>Precio:</strong>
                                </label>{" "}
                                {currentViaje.precio }
                            </div>

                            <Link
                                to={"/viaje/" + currentViaje.id}
                                className="badge badge-warning"
                            >
                                Edit
                            </Link>
                        </div>
                    ) : (
                        <div>
                            <br />
                            <p>Porfavor haga click en un viaje para mas opciones..</p>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}
