import React, { Component } from "react";
import ViajeroDataService from "../services/viajero.service";

export default class Viajero extends Component {
  constructor(props) {
    super(props);
    this.onChangeNombre = this.onChangeNombre.bind(this);
    this.onChangeCedula = this.onChangeCedula.bind(this);
    this.onChangeFechaNacimiento = this.onChangeFechaNacimiento.bind(this);
    this.onChangeTelefono = this.onChangeTelefono.bind(this);
    this.getViajero = this.getViajero.bind(this);
    this.updateViajero = this.updateViajero.bind(this);
    this.deleteViajero = this.deleteViajero.bind(this);

    this.state = {
      currentViajero: {
        id: null,
        nombre: "",
        cedula: "",
        fecha_nacimiento: "",
        telefono: "",
      },
    };
  }

  componentDidMount() {
    this.getViajero(this.props.match.params.id);
  }

  onChangeNombre(e) {
    const nombre = e.target.value;

    this.setState(function(prevState) {
      return {
        currentViajero: {
          ...prevState.currentViajero,
          nombre: nombre
        }
      };
    });
  }
  onChangeCedula(e) {
    const cedula = e.target.value;

    this.setState(function(prevState) {
      return {
        currentViajero: {
          ...prevState.currentViajero,
          cedula: cedula
        }
      };
    });
  }
  onChangeFechaNacimiento(e) {
    const fechaNacimiento = e.target.value;

    this.setState(function(prevState) {
      return {
        currentViajero: {
          ...prevState.currentViajero,
          fecha_nacimiento: fechaNacimiento
        }
      };
    });
  }
  onChangeTelefono(e) {
    const telefono = e.target.value;

    this.setState(function(prevState) {
      return {
        currentViajero: {
          ...prevState.currentViajero,
          telefono: telefono
        }
      };
    });
  }

  getViajero(id) {
    ViajeroDataService.get(id)
      .then(response => {
        this.setState({
          currentViajero: response.data
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  updateViajero() {
    ViajeroDataService.update(
      this.state.currentViajero.id,
      this.state.currentViajero
    )
      .then(response => {
        console.log(response.data);
        this.setState({
          message: "El Viajero fue actualizado exitosamente!"
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  deleteViajero() {
    ViajeroDataService.delete(this.state.currentViajero.id)
      .then(response => {
        console.log(response.data);
        this.props.history.push('/viajeros')
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    const { currentViajero } = this.state;

    return (
      <div>
        {currentViajero ? (
          <div className="edit-form">
            <h4>Viajero</h4>
            <form>
              <div className="form-group">
                <label htmlFor="nombre">Nombre</label>
                <input
                  type="text"
                  className="form-control"
                  id="nombre"
                  value={currentViajero.nombre}
                  onChange={this.onChangeNombre}
                />
              </div>
              <div className="form-group">
                <label htmlFor="cedula">Cedula</label>
                <input
                  type="text"
                  className="form-control"
                  id="cedula"
                  value={currentViajero.cedula}
                  onChange={this.onChangeCedula}
                />
              </div>

              <div className="form-group">
                <label htmlFor="fechaNacimiento">fecha de Nacimiento</label>
                <input
                    type="text"
                    className="form-control"
                    id="fechaNacimiento"
                    value={currentViajero.fecha_nacimiento}
                    onChange={this.onChangeFechaNacimiento}
                />
              </div>
              <div className="form-group">
                <label htmlFor="telefono">Telefono</label>
                <input
                    type="text"
                    className="form-control"
                    id="telefono"
                    value={currentViajero.telefono}
                    onChange={this.onChangeTelefono}
                />
              </div>
            </form>
            <button
              className="badge badge-danger mr-2"
              onClick={this.deleteViajero}
            >
              Delete
            </button>

            <button
              type="submit"
              className="badge badge-success"
              onClick={this.updateViajero}
            >
              Update
            </button>
          </div>
        ) : (
          <div>
            <br />
            <p>Porfavor haga click en un viajero para mas opciones..</p>
          </div>
        )}
      </div>
    );
  }
}
