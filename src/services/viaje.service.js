import http from "../http-common";

class ViajeDataService {
    getAll() {
        return http.get("/viajes");
    }

    get(id) {
        return http.get(`/viaje/${id}`);
    }

    create(data) {
        return http.post("/viaje", data);
    }

    update(id, data) {
        return http.put(`/viaje/${id}`, data);
    }

    delete(id) {
        return http.delete(`/viaje/${id}`);
    }
}

export default new ViajeDataService();