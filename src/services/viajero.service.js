import http from "../http-common";

class ViajeroDataService {
  getAll() {
    return http.get("/viajeros");
  }

  get(id) {
    return http.get(`/viajero/${id}`);
  }

  create(data) {
    return http.post("/viajero", data);
  }

  update(id, data) {
    return http.put(`/viajero/${id}`, data);
  }

  delete(id) {
    return http.delete(`/viajero/${id}`);
  }
}

export default new ViajeroDataService();