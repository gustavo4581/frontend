import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import AddViajero from "./components/add-viajero";
import Viajero from "./components/viajero";
import Viaje from "./components/viaje";
import ViajeroList from "./components/viajero-list";
import AddViaje from "./components/add-viaje";
import ViajesList from "./components/viaje-list";



class App extends Component {
    render() {
        return (
            <div>
                <nav className="navbar navbar-expand navbar-dark bg-dark">
                    <Link to={"/viajeros"} className="navbar-brand">
                        APP
                    </Link>
                    <div className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <Link to={"/Viajeros"} className="nav-link">
                                Viajeros
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to={"/add-viajero"} className="nav-link">
                                Agregar Viajero
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to={"/Viajes"} className="nav-link">
                                Viajes
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to={"/add-viaje"} className="nav-link">
                                Agegar Viaje
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to={"/add-viaje"} className="nav-link">
                                Gestion de viajes
                            </Link>
                        </li>
                    </div>
                </nav>

                <div className="container mt-3">
                    <Switch>
                        <Route exact path={["/", "/viajeros"]} component={ViajeroList} />
                        <Route exact path={["/", "/viajes"]} component={ViajesList} />
                        <Route exact path="/add-viajero" component={AddViajero} />
                        <Route exact path="/add-viaje" component={AddViaje} />
                        <Route path="/viajero/:id" component={Viajero} />
                        <Route path="/viaje/:id" component={Viaje} />
                    </Switch>
                </div>
            </div>
        );
    }
}

export default App;
