FROM node:alpine

WORKDIR /app

COPY ./frontend/package.json /app


RUN npm install -g react-scripts

RUN npm install

COPY ./frontend /app

CMD ["npm", "run", "start"]
